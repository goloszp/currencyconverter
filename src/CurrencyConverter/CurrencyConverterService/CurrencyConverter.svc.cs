﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;

namespace CurrencyConverterService
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "Service1" in code, svc and config file together.
    // NOTE: In order to launch WCF Test Client for testing this service, please select Service1.svc or Service1.svc.cs at the Solution Explorer and start debugging.
    public class CurrencyConverter : ICurrencyConverter
    {
        private string dollarsWording = "dollars";
        private string centsWording = "cents";
        private string convertedCurrencyToWords;
        private int orderOfMagnitude;
        private double currentNumber;
        private double startingNumber;
        private double restOfModuloToProcess;
        private Dictionary<string, string[]> numbersToWordMap = new Dictionary<string, string[]>()
        {
            {"units", new string[] {"zero", "one", "two", "three", "four", "five", "six", "seven", "eight", "nine","ten", "eleven", "twelve", "thirteen", "fourteen", "fifteen", "sixteen", "seventeen", "eighteen", "nineteen"}},
            {"decimal", new string[] {"zero", "ten", "twenty", "thirty", "forty", "fifty", "sixty", "seventy", "eighty", "ninety", "twenty"} },
            {"magnitude", new string[] {"thousand", "million"} }
        };

        public string ConvertCurrency(int dollars = 0, int cents = 0)
        {
            startingNumber = dollars;
            orderOfMagnitude = (int)Math.Floor(Math.Log(Convert.ToDouble(dollars), 1000.00));
            currentNumber = dollars / Math.Pow(1000, orderOfMagnitude);
            restOfModuloToProcess = dollars % Math.Pow(1000, orderOfMagnitude);
            // Dollars Converter
            for (int i = orderOfMagnitude; i >= 0; i--)
            {
                currentNumber = startingNumber / Math.Pow(1000, i);
                restOfModuloToProcess = startingNumber % Math.Pow(1000, i);
                if (currentNumber >= 100)
                    convertedCurrencyToWords += convertingHundreds((int)currentNumber);
                convertedCurrencyToWords += upToHundredConvertion((int)currentNumber % 100);
                if (i != 0)
                    convertedCurrencyToWords += numbersToWordMap["magnitude"][i - 1]+" ";
                startingNumber = restOfModuloToProcess;
            };

            if (dollars == 1)
                dollarsWording = "dollar";
            if (dollars == 0)
                convertedCurrencyToWords += "zero ";
            convertedCurrencyToWords += dollarsWording;

            // Cents converter
            if (cents != 0)
            {
                if (cents == 1)
                    centsWording = "cent";

                convertedCurrencyToWords += " and " + upToHundredConvertion(cents) + centsWording;
            }
            return string.Format(convertedCurrencyToWords);
        }
        private string convertingHundreds(int numberWithHundreds)
        {
            return numbersToWordMap["units"][(int)Math.Floor(currentNumber / 100)]+" hundred ";
        }
        private string upToHundredConvertion(int upToHundred)
        {
            string convertedNumber = "";
            if (upToHundred >= 21 && upToHundred < 100)
            {
                if (upToHundred % 10 == 0)
                    convertedNumber += numbersToWordMap["decimal"][upToHundred / 10] + " ";
                else
                    convertedNumber += numbersToWordMap["decimal"][upToHundred / 10] + "-" + numbersToWordMap["units"][upToHundred % 10] + " ";
            };

            if (upToHundred < 21 && upToHundred >= 1)
                convertedNumber += numbersToWordMap["units"][upToHundred] + " ";
            return convertedNumber;
        }
    }
}
