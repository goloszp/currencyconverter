﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace CurrencyConverterService.Tests
{
    [TestClass]
    public class CurrencyConverterTests
    {
        [TestMethod]
        public void Input0()
        {
            var dollars = 0;
            var cc = new CurrencyConverter();
            var response = cc.ConvertCurrency(dollars, 0);
            Assert.AreEqual("zero dollars", response);
        }

        [TestMethod]
        public void Input1()
        {
            var dollars = 1;
            var cc = new CurrencyConverter();
            var response = cc.ConvertCurrency(dollars, 0);
            Assert.AreEqual("one dollar", response);
        }

        [TestMethod]
        public void Input25and10cents()
        {
            var dollars = 25;
            var cents = 10;
            var cc = new CurrencyConverter();
            var response = cc.ConvertCurrency(dollars, cents);
            Assert.AreEqual("twenty-five dollars and ten cents", response);

        }

        [TestMethod]
        public void Input1cent()
        {
            var dollars = 0;
            var cents = 1;
            var cc = new CurrencyConverter();
            var response = cc.ConvertCurrency(dollars, cents);
            Assert.AreEqual("zero dollars and one cent", response);
        }

        [TestMethod]
        public void Input45k100()
        {
            var dollars = 45100;
            var cents = 0;
            var cc = new CurrencyConverter();
            var response = cc.ConvertCurrency(dollars, cents);
            Assert.AreEqual("forty-five thousand one hundred dollars", response);
        }

        [TestMethod]
        public void Input999999999dollar99cent()
        {
            var dollars = 999999999;
            var cents = 99;
            var cc = new CurrencyConverter();
            var response = cc.ConvertCurrency(dollars, cents);
            Assert.AreEqual("nine hundred ninety-nine million nine hundred ninety-nine thousand nine hundred ninety-nine dollars and ninety-nine cents", response);
        }
    }
}
